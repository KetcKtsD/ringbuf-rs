#[derive(Debug, Clone, thiserror::Error)]
pub enum Error {
    #[error("buffer overflow. state={0:?}")]
    Overflow(State),
    #[error("buffer underflow. state={0:?}")]
    Underflow(State),
    #[error("the pair is dropped")]
    Dropped,
}

#[derive(Debug, Clone)]
pub struct State {
    pub head: usize,
    pub tail: usize,
    pub capacity: usize,
}
