#![crate_name = "ringbuf"]
#![crate_type = "lib"]

pub mod error;

pub mod spsc;

mod pow2;

pub type Result<T> = std::result::Result<T, error::Error>;
