use crate::spsc::{Consumer, Producer};

pub trait ConsumerExt<T> {
    fn pop_to_slice(&self, target: &mut [T]) -> usize;
}

impl<T> ConsumerExt<T> for Consumer<T>
where
    T: 'static,
{
    fn pop_to_slice(&self, target: &mut [T]) -> usize {
        let mut i = 0;
        for x in target.iter_mut() {
            let Ok(y) = self.pop() else {
                return i;
            };
            *x = y;
            i += 1;
        }
        i
    }
}

pub trait ProducerExt<T> {
    fn push_iter<I>(&self, iter: I) -> usize
    where
        I: Iterator<Item = T>;
}

impl<T> ProducerExt<T> for Producer<T>
where
    T: 'static,
{
    fn push_iter<I>(&self, iter: I) -> usize
    where
        I: Iterator<Item = T>,
    {
        let mut i = 0;
        for x in iter {
            if self.push(x).is_err() {
                break;
            }
            i += 1;
        }
        i
    }
}
