use crate::error::State;
use std::sync::atomic::Ordering;

use super::*;

/// The channel for [Bounded].
///
/// It is a tuple of [Producer] and [Consumer].
pub type Channel<T> = (Producer<T>, Consumer<T>);

/// The producer side of the channel for [Bounded].
#[derive(Debug)]
pub struct Producer<T> {
    refer: Arc<Bounded<T>>,
}

impl<T> Producer<T>
where
    T: 'static,
{
    #[inline(always)]
    pub(crate) fn new(refer: Arc<Bounded<T>>) -> Self {
        Self { refer }
    }

    /// Push an element to the ring buffer.
    ///
    /// # Errors
    /// - [Error::Overflow] if the ring buffer is full.
    /// - [Error::Dropped] if the consumer is dropped.
    pub fn push(&self, elem: T) -> crate::Result<()> {
        let refer = &self.refer;

        let write = refer.write.load(Ordering::Relaxed);
        let read = refer.read.load(Ordering::Acquire);

        if write - read == refer.cap {
            return Err(Error::Overflow(State {
                head: read,
                tail: write,
                capacity: refer.cap,
            }));
        }

        refer.put(write & refer.rest_mask, elem);

        refer.write.store(write + 1, Ordering::Release);
        Ok(())
    }

    /// Get the number of elements in the ring buffer.
    /// Even if you call this method, the number of elements may change immediately.
    pub fn len(&self) -> usize {
        let refer = &self.refer;
        let c_tail = refer.write.load(Ordering::Relaxed);
        let c_head = refer.read.load(Ordering::Relaxed);
        c_tail - c_head
    }

    /// Check if the ring buffer is empty.
    /// Even if you call this method, the number of elements may change immediately.
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Check if the ring buffer is full.
    /// Even if you call this method, the number of elements may change immediately.
    pub fn is_full(&self) -> bool {
        self.len() == self.refer.cap
    }

    /// Clear all the elements accumulated in Producer.
    pub fn clear(&self) {
        let refer = &self.refer;
        let head = refer.read.load(Ordering::Relaxed);
        refer.write.store(head, Ordering::Release);
    }
}

/// The consumer side of the channel for [Bounded].
#[derive(Debug)]
pub struct Consumer<T> {
    refer: Arc<Bounded<T>>,
}

impl<T> Consumer<T>
where
    T: 'static,
{
    #[inline(always)]
    pub(crate) fn new(refer: Arc<Bounded<T>>) -> Self {
        Self { refer }
    }

    /// Pop an element from the ring buffer.
    ///
    /// # Errors
    /// - [Error::Underflow] if the ring buffer is empty.
    /// - [Error::Dropped] if the producer is dropped.
    pub fn pop(&self) -> crate::Result<T> {
        let refer = &self.refer;

        let read = refer.read.load(Ordering::Relaxed);
        let write = refer.write.load(Ordering::Acquire);

        if write == read {
            return Err(Error::Underflow(State {
                head: read,
                tail: write,
                capacity: refer.cap,
            }));
        };

        let elem = refer.get(read & refer.rest_mask);

        refer.read.store(read + 1, Ordering::Release);
        Ok(elem)
    }

    /// Get the number of elements in the ring buffer.
    /// Even if you call this method, the number of elements may change immediately.
    pub fn len(&self) -> usize {
        let refer = &self.refer;
        let c_head = refer.read.load(Ordering::Relaxed);
        let c_tail = refer.write.load(Ordering::Relaxed);
        c_tail - c_head
    }

    /// Check if the ring buffer is empty.
    /// Even if you call this method, the number of elements may change immediately.
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Check if the ring buffer is full.
    /// Even if you call this method, the number of elements may change immediately.
    pub fn is_full(&self) -> bool {
        self.len() == self.refer.cap
    }

    /// Clear all the elements accumulated in Consumer.
    pub fn clear(&self) {
        let refer = &self.refer;
        let tail = refer.write.load(Ordering::Relaxed);
        refer.read.store(tail, Ordering::Release);
    }

    pub fn iter(&self) -> Iter<'_, T> {
        Iter { inner: self }
    }
}

pub struct Iter<'a, T> {
    inner: &'a Consumer<T>,
}

impl<'a, T> Iterator for Iter<'a, T>
where
    T: 'static,
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.pop().ok()
    }
}
