use std::cell::UnsafeCell;
use std::fmt::{Debug, Formatter};
use std::ops::Deref;
use std::sync::atomic::AtomicUsize;
use std::sync::{Arc, Mutex as StdMutex};

pub use split::*;

use crate::error::Error;
use crate::pow2;

pub mod ext;
mod split;

#[cfg(test)]
mod test;

/// Lockless, single-producer, single-consumer, bounded, ring buffer.
///
/// The type [T] must be bound to [Copy].
///
/// # Safety
/// Operating with patterns other than `SPSC` may cause undefined behavior.
///
/// # Example
///
/// ```
/// use tokio::spawn;
///
/// use ringbuf::spsc::channel;
///
///
/// #[tokio::main(flavor = "multi_thread", worker_threads = 2)]
/// async fn main() {
/// let (prod, cons) = channel(1024);
/// let repeat = 1000;
///
/// let fut1 = spawn(async move {
///     for i in 0..repeat {
///         while prod.push(i).is_err() {}
///     }
/// });
///
/// let fut2 = spawn(async move {
///     for i in 0..repeat {
///         let mut result = cons.pop();
///         while result.is_err() {
///             result = cons.pop();
///         }
///         assert_eq!(result.unwrap(), i);
///     }
/// });
/// }
/// ```
pub fn channel<T>(size: usize) -> Channel<T>
where
    T: 'static,
{
    let bounded = Arc::new(Bounded::<T>::allocate(size));
    let (prod, cons) = bounded.try_split().unwrap();
    (prod, cons)
}

/// The bounded ring buffer.
#[derive(Debug)]
pub(crate) struct Bounded<T> {
    buf: Vec<UnsafeCell<Option<T>>>,
    // Avoid false sharing
    read: LargeBox<AtomicUsize>,
    write: LargeBox<AtomicUsize>,

    cap: usize,
    rest_mask: usize,
    already_split: StdMutex<bool>,
}

impl<T> Bounded<T>
where
    T: 'static,
{
    /// Allocate a new bounded ring buffer.
    ///
    /// # Args
    /// - `size`: The size of the ring buffer.
    /// The actual size of the ring buffer will be the nearest larger power of 2.
    pub fn allocate(size: usize) -> Self {
        let size = pow2::convert_to_nearest_larger_pow2(size);
        let mut buf = Vec::with_capacity(size);
        (0..size).for_each(|_| buf.push(Default::default()));
        Self {
            buf,
            read: Default::default(),
            write: Default::default(),
            cap: size,
            rest_mask: size - 1,
            already_split: Default::default(),
        }
    }

    pub fn try_split(self: Arc<Self>) -> Option<Channel<T>> {
        let mut already_split = self.already_split.lock().unwrap();
        if *already_split {
            return None;
        }
        *already_split = true;

        Some((Producer::new(self.clone()), Consumer::new(self.clone())))
    }

    pub fn put(&self, i: usize, value: T) {
        let ptr = self.buf[i].get();
        unsafe { (*ptr).replace(value) };
    }

    pub fn get(&self, i: usize) -> T {
        let ptr = self.buf[i].get();
        unsafe { (*ptr).take().unwrap() }
    }
}

unsafe impl<T> Send for Bounded<T> {}
unsafe impl<T> Sync for Bounded<T> {}

/// Reduce the probability of false sharing
struct LargeBox<T> {
    inner: T,
    // padding 64 bytes
    _1: u64,
    _2: u64,
    _3: u64,
    _4: u64,
    _5: u64,
    _6: u64,
    _7: u64,
    _8: u64,
}

impl<T> Deref for LargeBox<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T> Default for LargeBox<T>
where
    T: Default,
{
    fn default() -> Self {
        Self {
            inner: Default::default(),
            _1: 0,
            _2: 0,
            _3: 0,
            _4: 0,
            _5: 0,
            _6: 0,
            _7: 0,
            _8: 0,
        }
    }
}

impl<T> Debug for LargeBox<T>
where
    T: Debug,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.inner.fmt(f)
    }
}
