use std::sync::atomic::Ordering;
use std::sync::Arc;

use macros::*;

use crate::spsc::channel;

use super::Bounded;

#[test]
fn check_allocate() {
    let bounded = Bounded::<u8>::allocate(31);
    assert_eq!(bounded.cap, 32);
    assert_eq!(bounded.read.load(Ordering::Relaxed), 0);
    assert_eq!(bounded.write.load(Ordering::Relaxed), 0);
    assert_eq!(bounded.rest_mask, 31);
}

#[tokio::test(flavor = "multi_thread")]
async fn check_try_split() {
    let bounded = Bounded::<u8>::allocate(31);
    let bounded = Arc::new(bounded);
    let b1 = bounded.clone();
    let b2 = bounded.clone();
    let fut1 = tokio::spawn(async { Bounded::try_split(b1) });
    let fut2 = tokio::spawn(async { Bounded::try_split(b2) });
    let (r1, r2) = await_all!(fut1, fut2);
    assert_ok!(r1);
    assert_ok!(r2);
    assert!(r1.unwrap().is_some() ^ r2.unwrap().is_some())
}

// noinspection DuplicatedCode
#[test]
fn check_push() {
    let bounded = Bounded::<u8>::allocate(8);
    let bounded = Arc::new(bounded);
    let (prod, cons) = Bounded::try_split(bounded.clone()).unwrap();

    for i in 1..=8 {
        assert_ok!(prod.push(i));
    }
    let result = prod.push(9);
    assert_err!(result);
    for (i, ele) in bounded.buf.iter().enumerate() {
        let ptr = ele.get();
        let v = unsafe { (*ptr).unwrap() };
        assert_eq!(i + 1, v as usize);
    }

    let _ = cons.pop();
    let _ = cons.pop();

    let r1 = prod.push(9);
    let r2 = prod.push(10);

    assert_ok!(r1);
    assert_ok!(r2);

    let expect = [9, 10, 3, 4, 5, 6, 7, 8];
    for (i, ele) in bounded.buf.iter().enumerate() {
        let ptr = ele.get();
        let v = unsafe { (*ptr).unwrap() };
        assert_eq!(expect[i], v);
    }
    assert_err!(prod.push(11));
}

#[test]
fn check_consumer_len() {
    let (prod, cons) = channel::<u8>(8);
    assert_eq!(cons.len(), 0);
    (0..8).for_each(|_| {
        let _ = prod.push(0);
    });
    assert_eq!(cons.len(), 8);
    assert_ok!(cons.pop());
    assert_eq!(cons.len(), 7);
    assert_ok!(cons.pop());
    assert_eq!(cons.len(), 6);
    assert_ok!(cons.pop());
    assert_eq!(cons.len(), 5);
    assert_ok!(cons.pop());
    assert_eq!(cons.len(), 4);
    assert_ok!(cons.pop());
    assert_eq!(cons.len(), 3);
    assert_ok!(cons.pop());
    assert_eq!(cons.len(), 2);
    assert_ok!(cons.pop());
    assert_eq!(cons.len(), 1);
    assert_ok!(cons.pop());
    assert_eq!(cons.len(), 0);
    assert_err!(cons.pop());
}

#[test]
fn check_consumer_is_empty() {
    let (prod, cons) = channel::<u8>(8);
    assert!(cons.is_empty());
    (0..8).for_each(|_| {
        let _ = prod.push(0);
    });
    assert!(!cons.is_empty());
    assert_ok!(cons.pop());
    assert!(!cons.is_empty());
    assert_ok!(cons.pop());
    assert!(!cons.is_empty());
    assert_ok!(cons.pop());
    assert!(!cons.is_empty());
    assert_ok!(cons.pop());
    assert!(!cons.is_empty());
    assert_ok!(cons.pop());
    assert!(!cons.is_empty());
    assert_ok!(cons.pop());
    assert!(!cons.is_empty());
    assert_ok!(cons.pop());
    assert!(!cons.is_empty());
    assert_ok!(cons.pop());
    assert!(cons.is_empty());
    assert_err!(cons.pop());
}

#[test]
fn check_consumer_is_full() {
    let (prod, cons) = channel::<u8>(8);
    assert!(!cons.is_full());
    (0..8).for_each(|_| {
        let _ = prod.push(0);
    });
    assert!(cons.is_full());
    assert_ok!(cons.pop());
    assert!(!cons.is_full());
    assert_ok!(cons.pop());
    assert!(!cons.is_full());
    assert_ok!(cons.pop());
    assert!(!cons.is_full());
    assert_ok!(cons.pop());
    assert!(!cons.is_full());
    assert_ok!(cons.pop());
    assert!(!cons.is_full());
    assert_ok!(cons.pop());
    assert!(!cons.is_full());
    assert_ok!(cons.pop());
    assert!(!cons.is_full());
    assert_ok!(cons.pop());
    assert!(!cons.is_full());
    assert_err!(cons.pop());
}

#[test]
fn check_consumer_clear() {
    let (prod, cons) = channel::<u8>(8);
    (0..8).for_each(|_| {
        let _ = prod.push(0);
    });
    prod.clear();
    (0..8).for_each(|_| {
        let _ = prod.push(0);
    });
    assert_ok!(cons.pop());
    prod.clear();
    assert_err!(cons.pop());
}

#[test]
fn check_pop() {
    let bounded = Bounded::<u8>::allocate(8);
    let bounded = Arc::new(bounded);
    let (prod, cons) = Bounded::try_split(bounded.clone()).unwrap();
    (0..8).for_each(|x| {
        let _ = prod.push(x + 1);
    });
    assert_eq!(cons.pop().unwrap(), 1);
    assert_eq!(cons.pop().unwrap(), 2);
    assert_eq!(cons.pop().unwrap(), 3);
    assert_eq!(cons.pop().unwrap(), 4);
    assert_eq!(cons.pop().unwrap(), 5);
    assert_eq!(cons.pop().unwrap(), 6);
    assert_eq!(cons.pop().unwrap(), 7);
    assert_eq!(cons.pop().unwrap(), 8);
    assert_err!(cons.pop());
    (9..=12).for_each(|x| {
        let _ = prod.push(x);
    });
    assert_eq!(cons.pop().unwrap(), 9);
    assert_eq!(cons.pop().unwrap(), 10);
    assert_eq!(cons.pop().unwrap(), 11);
    assert_eq!(cons.pop().unwrap(), 12);
    assert_err!(cons.pop());
    (13..=20).for_each(|x| {
        let _ = prod.push(x);
    });
    assert_eq!(cons.pop().unwrap(), 13);
    assert_eq!(cons.pop().unwrap(), 14);
    assert_eq!(cons.pop().unwrap(), 15);
    assert_eq!(cons.pop().unwrap(), 16);
    assert_eq!(cons.pop().unwrap(), 17);
    assert_eq!(cons.pop().unwrap(), 18);
    assert_eq!(cons.pop().unwrap(), 19);
    assert_eq!(cons.pop().unwrap(), 20);
    assert_err!(cons.pop());
}

#[test]
fn check_producer_len() {
    let (prod, cons) = channel::<u8>(8);
    assert_eq!(prod.len(), 0);
    (0..8).for_each(|_| {
        let _ = prod.push(0);
    });
    assert_eq!(prod.len(), 8);
    assert_ok!(cons.pop());
    assert_eq!(prod.len(), 7);
    assert_ok!(cons.pop());
    assert_eq!(prod.len(), 6);
    assert_ok!(cons.pop());
    assert_eq!(prod.len(), 5);
    assert_ok!(cons.pop());
    assert_eq!(prod.len(), 4);
    assert_ok!(cons.pop());
    assert_eq!(prod.len(), 3);
    assert_ok!(cons.pop());
    assert_eq!(prod.len(), 2);
    assert_ok!(cons.pop());
    assert_eq!(prod.len(), 1);
    assert_ok!(cons.pop());
    assert_eq!(prod.len(), 0);
    assert_err!(cons.pop());
}

#[test]
fn check_producer_is_empty() {
    let (prod, cons) = channel::<u8>(8);
    assert!(prod.is_empty());
    (0..8).for_each(|_| {
        let _ = prod.push(0);
    });
    assert!(!prod.is_empty());
    assert_ok!(cons.pop());
    assert!(!prod.is_empty());
    assert_ok!(cons.pop());
    assert!(!prod.is_empty());
    assert_ok!(cons.pop());
    assert!(!prod.is_empty());
    assert_ok!(cons.pop());
    assert!(!prod.is_empty());
    assert_ok!(cons.pop());
    assert!(!prod.is_empty());
    assert_ok!(cons.pop());
    assert!(!prod.is_empty());
    assert_ok!(cons.pop());
    assert!(!prod.is_empty());
    assert_ok!(cons.pop());
    assert!(prod.is_empty());
    assert_err!(cons.pop());
}

#[test]
fn check_producer_is_full() {
    let (prod, cons) = channel::<u8>(8);
    assert!(!prod.is_full());
    (0..8).for_each(|_| {
        let _ = prod.push(0);
    });
    assert!(prod.is_full());
    assert_ok!(cons.pop());
    assert!(!prod.is_full());
    assert_ok!(cons.pop());
    assert!(!prod.is_full());
    assert_ok!(cons.pop());
    assert!(!prod.is_full());
    assert_ok!(cons.pop());
    assert!(!prod.is_full());
    assert_ok!(cons.pop());
    assert!(!prod.is_full());
    assert_ok!(cons.pop());
    assert!(!prod.is_full());
    assert_ok!(cons.pop());
    assert!(!prod.is_full());
    assert_ok!(cons.pop());
    assert!(!prod.is_full());
    assert_err!(cons.pop());
}

#[test]
fn check_producer_clear() {
    let (prod, cons) = channel::<u8>(8);
    (0..8).for_each(|_| {
        let _ = prod.push(0);
    });
    cons.clear();
    (0..8).for_each(|_| {
        let _ = prod.push(0);
    });
    assert_ok!(cons.pop());
    cons.clear();
    assert_err!(cons.pop());
}
struct I(i64);
struct A {
    i: I,
    _v: Vec<i64>,
}

#[tokio::test(flavor = "multi_thread")]
async fn spsc_push_pop() {
    let repeat: i64 = 5000000;
    let bounded = Bounded::<Box<A>>::allocate(1024);
    let bounded = Arc::new(bounded);
    let (prod, cons) = bounded.try_split().unwrap();

    let fut1 = tokio::spawn(async move {
        for i in 0..repeat {
            while prod
                .push(Box::new(A {
                    i: I(i),
                    _v: Vec::new(),
                }))
                .is_err()
            {}
        }
    });

    let fut2 = tokio::spawn(async move {
        for i in 0..repeat {
            let mut result = cons.pop();
            while result.is_err() {
                result = cons.pop();
            }
            let result = result.unwrap();
            assert_eq!(result.i.0, i);
        }
    });
    assert_ok!(fut1.await);
    fut2.await.unwrap();
}
