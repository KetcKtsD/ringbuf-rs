/// Convert to the nearest power of 2 number greater than the given value
///
/// # Panics
/// - input value is 0.
/// - converted value is greater than the isize::MAX bytes.
#[inline(always)]
pub(crate) fn convert_to_nearest_larger_pow2(size: usize) -> usize {
    let mut n = size - 1;
    let mut pow2 = 1;
    while n != 0 {
        n >>= 1;
        pow2 <<= 1;
    }
    pow2
}

#[cfg(test)]
#[test]
fn check_convert_to_nearest_larger_pow2() {
    let table = [
        (1, 1),
        (2, 2),
        (3, 4),
        (4, 4),
        (5, 8),
        (7, 8),
        (8, 8),
        (9, 16),
        (15, 16),
        (16, 16),
        (65, 128),
        (127, 128),
        (128, 128),
        (129, 256),
    ];
    for (input, expect) in table {
        assert_eq!(
            convert_to_nearest_larger_pow2(input),
            expect,
            "input: {}, except: {}",
            input,
            expect
        );
    }
}
