#![crate_name = "macros"]
#![crate_type = "lib"]

pub mod error {
    #[macro_export]
    macro_rules! bail {
        ($x:expr) => {
            return std::result::Result::Err($x);
        };
    }
}

pub mod future {
    #[macro_export]
    macro_rules! await_all {
        ($( $x:expr ),*) => {
            ($($x.await,)*)
        };
    }
}

pub mod test {
    #[macro_export]
    macro_rules! assert_ok {
        ($x:expr) => {
            assert!($x.is_ok())
        };
    }

    #[macro_export]
    macro_rules! assert_err {
        ($x:expr) => {
            assert!($x.is_err())
        };
    }

    #[macro_export]
    macro_rules! assert_some {
        ($x:expr) => {
            assert!($x.is_some())
        };
    }

    #[macro_export]
    macro_rules! assert_none {
        ($x:expr) => {
            assert!($x.is_none())
        };
    }

    #[macro_export]
    macro_rules! assert_not {
        ($x:expr) => {
            assert!(!$x)
        };
    }
}
