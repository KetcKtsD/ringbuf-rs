# Introduction

This is a simple Lock-free, Single-Producer Single-Consumer (SPSC) circular buffer implementation in Rust.

# Usage

```rust
#[tokio::main]
async fn main() {
    let (prod, cons) = channel(1024);
    let repeat = 1000;
    let fut1 = spawn(async move {
        for i in 0..repeat {
            while prod.push(i).is_err() {}
        }
    });
    let fut2 = spawn(async move {
        for i in 0..repeat {
            let mut result = cons.pop();
            while result.is_err() {
                result = cons.pop();
            }
            assert_eq!(result.unwrap(), i);
        }
    });
}
```
